import { Component, Input, OnInit } from '@angular/core';
import { GameDetails } from '../models/game-details';

@Component({
  selector: 'xclb-game-item',
  templateUrl: './game-item.component.html',
  styleUrls: ['./game-item.component.css']
})

export class GameItemComponent implements OnInit {

  @Input() game: GameDetails;

  ngOnInit() {
    console.log(this.game);
  }

}
