import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GameItemComponent } from './game-item/game-item.component';
import { GameShowComponent } from './game-show/game-show.component';
import { SearchComponent } from './search/search.component';
import { GameService } from './shared/game.service';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CherryHttpInterceptor } from './shared/cherry-http.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CarouselComponent,
    FooterComponent,
    NavbarComponent,
    GameItemComponent,
    GameShowComponent,
    SearchComponent,
    PathNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
providers: [
  GameService,
  {provide: HTTP_INTERCEPTORS, useClass: CherryHttpInterceptor, multi: true}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
