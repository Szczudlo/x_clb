import { Component, OnInit } from '@angular/core';
import { GameService } from '../shared/game.service';
import { ActivatedRoute } from '@angular/router';
import { GameDetails } from '../models/game-details';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'xclb-game-show',
  templateUrl: './game-show.component.html',
  styleUrls: ['./game-show.component.css']
})
export class GameShowComponent implements OnInit {
  game: GameDetails[];

  constructor(private route: ActivatedRoute, private gameService: GameService) { }

  ngOnInit() {
    const gId: string = this.route.snapshot.params[`gameId`];
    this.gameService.getGames().subscribe(
      g => this.game = g.filter(x => x.id === gId),
      (err: HttpErrorResponse) => console.log(`Error: ${err}`)
    );
  }

}
