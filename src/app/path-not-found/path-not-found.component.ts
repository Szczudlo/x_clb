import { Component } from '@angular/core';

@Component({
  selector: 'xclb-path-not-found',
  templateUrl: './path-not-found.component.html',
  styleUrls: ['./path-not-found.component.css']
})
export class PathNotFoundComponent { }
