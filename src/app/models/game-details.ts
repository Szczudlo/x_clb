export interface GameDetails {
  id: string;
  name: string;
  thumbnail: string;
}
