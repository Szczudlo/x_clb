export interface GameCategory {
  name: string;
  order: number;
  slug: string;
}
