import { Injectable } from '@angular/core';
import { GameCategory } from '../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface GameCategoryDTO {
  _embedded: {
    game_categories: GameCategory[];
  };
}

const cherryApi = 'https://staging-frontapi.cherrytech.com/game-categories';

@Injectable({
  providedIn: 'root'
})
export class GameCategoryService {

  constructor(private httpClient: HttpClient) { }

  getCategories(): Observable<GameCategory[]> {
    return this.httpClient.get<GameCategoryDTO>(cherryApi).pipe(map(g => g._embedded.game_categories));
  }
}
