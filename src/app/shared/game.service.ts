import { Injectable } from '@angular/core';
import { GameDetails } from '../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface GameDTO {
  _embedded: {
    games: GameDetails[];
  };
}

const cherryApi = 'https://staging-frontapi.cherrytech.com/games';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private httpClient: HttpClient) { }

  getGames(): Observable<GameDetails[]> {
    return this.httpClient.get<GameDTO>(cherryApi).pipe(map(g => g._embedded.games));
  }
}
