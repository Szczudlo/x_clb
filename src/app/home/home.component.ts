import { Component, OnInit } from '@angular/core';
import { GameCategoryService } from '../shared/game-category.service';
import { GameService } from '../shared/game.service';
import { Observable } from 'rxjs';
import { GameDetails } from '../models/game-details';

@Component({
  selector: 'xclb-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  games: Observable<GameDetails[]>;

  constructor(private gameService: GameService, private gameCategoryService: GameCategoryService) { }

  ngOnInit() {
    this.games = this.gameService.getGames();
  }

}
