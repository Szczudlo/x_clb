import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GameShowComponent } from './game-show/game-show.component';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'games/:gameId', component: GameShowComponent },
  { path: '**', component: PathNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
